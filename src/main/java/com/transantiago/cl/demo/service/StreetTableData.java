package com.transantiago.cl.demo.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * Created by wvalera on 3/16/17.
 */
@RestController
@RequestMapping("/getStreetTableData")
public class StreetTableData {

    @Autowired
    private RestTemplate restTemplate;

    private static final String SERVICE_URL = "http://200.9.100.91/onlinegps/timePerStreet/getStreetTableData";
    private static final Logger log = Logger.getLogger(StreetTableData.class);

    @RequestMapping(value = "", produces = "application/json")
    public Object getTableData(@RequestParam(value = "axisId", required = false, defaultValue = "") String axisId) {
       try{
           Object object;
           if(axisId.isEmpty()) {
               object = restTemplate.getForObject(SERVICE_URL, Object.class);
           }else {
               object = restTemplate.getForObject(SERVICE_URL + "?axisId={axisId}", Object.class, axisId);
           }
           return object;
       }catch(Exception e){
           log.error("", e);
       }
        return null;
    }

}
