package com.transantiago.cl.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * Created by dokannon on 21-03-17.
 */
@RestController
@RequestMapping("/getMacroStreetTableData")
public class MacroStreetTableData {

        @Autowired
        private RestTemplate restTemplate;

        @RequestMapping(value = "", produces = "application/json")
        public Object getTableData() {
            try{
                return restTemplate.getForObject("http://200.9.100.91/onlinegps/timePerStreet/getMacroStreetTableData", Object.class);
            }catch(Exception e){
                return null;
            }

        }


}
