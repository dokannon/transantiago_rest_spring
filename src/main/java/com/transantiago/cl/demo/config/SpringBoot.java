package com.transantiago.cl.demo.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * Created by wvalera on 3/16/17.
 */
@SpringBootApplication(scanBasePackages = {"com.transantiago.cl.demo"})
public class SpringBoot extends SpringBootServletInitializer {
    public static void main(String... args) {
        SpringApplication.run(SpringBoot.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(SpringBoot.class);
    }
}